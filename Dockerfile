FROM openjdk:8	
EXPOSE 8080
ADD target/serverless.jar serverless.jar
ENTRYPOINT ["java","-jar","serverless.jar"]