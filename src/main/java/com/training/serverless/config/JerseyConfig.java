package com.training.serverless.config;



import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.training.serverless.controller.Controller;

@Component
public class JerseyConfig extends ResourceConfig 
{
    public JerseyConfig() 
    {
        register(Controller.class);
    	//packages("com.training.serverless.controller");
    }
}