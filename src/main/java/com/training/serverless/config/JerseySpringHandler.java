package com.training.serverless.config;

import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.amazonaws.serverless.proxy.internal.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.internal.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.jersey.JerseyLambdaContainerHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class JerseySpringHandler implements RequestHandler<AwsProxyRequest, AwsProxyResponse> {

    private JerseyLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler;


    public JerseySpringHandler() {
        // create Spring context
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(SpringConfig.class);
        context.refresh();

        // use Spring bean of JerseyResourceConfig to have spring supported resources
        JerseyConfig resourceConfig = context.getBean(JerseyConfig.class);
        handler = JerseyLambdaContainerHandler.getAwsProxyHandler(resourceConfig);
    }


    @Override
    public AwsProxyResponse handleRequest(AwsProxyRequest awsProxyRequest, Context context) {
        return handler.proxy(awsProxyRequest, context);
    }


	

}