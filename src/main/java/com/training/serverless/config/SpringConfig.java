package com.training.serverless.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.training.serverless")
public class SpringConfig {

}