package com.training.serverless.controller;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;

import com.training.serverless.exception.RecordNotFoundException;
import com.training.serverless.model.AuthRequest;
import com.training.serverless.model.EmployeeEntity;
import com.training.serverless.model.User;
import com.training.serverless.service.EmployeeService;
import com.training.serverless.service.UserService;
import com.training.serverless.util.JwtUtil;
 

@Path("/api")
public class Controller
{
    @Autowired
    EmployeeService service;
  
	@Autowired
	UserService userService;

	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@GET
	@Path("/check")
	public String check() {		
		return "OK";
	}
	
	@POST
	@Path(value="/register")
	@Consumes("application/json")
	@Produces("application/json")
	public ResponseEntity<User> saveUser(@RequestBody User user) throws Exception {
		
		return ResponseEntity.ok(userService.createUser(user));
	}

	@POST
	@Path(value="/authenticate")
	public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));
		} catch (Exception ex) {
			throw new Exception("inavalid username/password");
		}
		return jwtUtil.generateToken(authRequest.getUserName());
	}
    
 
    @GET
    @Path("/employee")
    @Produces("application/json")
    public ResponseEntity<List<EmployeeEntity>> getAllEmployees() {
        List<EmployeeEntity> list = service.getAllEmployees();
        return new ResponseEntity<List<EmployeeEntity>>(list, HttpStatus.OK);
    }
    
 
    @GET
    @Path(value="/employee/{id}")
    @Produces("application/json")
    public ResponseEntity<EmployeeEntity> getEmployeeById(@PathParam("id") Long id)
                                                    throws RecordNotFoundException {
        EmployeeEntity entity = service.getEmployeeById(id);
 
        return new ResponseEntity<EmployeeEntity>(entity, HttpStatus.OK);
    }
 
    @POST
    @Path("/employee")
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<EmployeeEntity> createEmployee(@RequestBody EmployeeEntity employee)
                                                    throws RecordNotFoundException {
    	System.out.println(employee);
        EmployeeEntity updated = service.createEmployee(employee);
        return new ResponseEntity<EmployeeEntity>(updated, HttpStatus.OK);
    }
    
    @PUT
    @Path("/employee")
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<EmployeeEntity> updateEmployee(@RequestBody EmployeeEntity employee)
                                                    throws RecordNotFoundException {
    	System.out.println(employee);
        EmployeeEntity updated = service.updateEmployee(employee);
        return new ResponseEntity<EmployeeEntity>(updated, HttpStatus.OK);
    }
 
    @DELETE 
    @Path(value="/employee/{id}")
    public HttpStatus deleteEmployeeById(@PathParam("id") Long id)
                                                    throws RecordNotFoundException {
        service.deleteEmployeeById(id);
        return HttpStatus.FORBIDDEN;
    }
 
}