package com.training.serverless.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.training.serverless.model.User;
import com.training.serverless.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userDao;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	public User createUser(User user) {
		User userEntity =new User();
		userEntity.setUserName(user.getUserName());
		userEntity.setPassword(bcryptEncoder.encode(user.getPassword()));
		userEntity.setEmail(user.getEmail());
		return userDao.save(userEntity);
	}
	
}
